# parcel-plugin-in
Environment aware JavaScript templating.

## Problem
Image you have a file called `config.js`, where you have some configuration constants:

``` javascript
module.exports = {
    BackendURL: "some-ip-address",
    MaxRetries: 12,
    ErrorPrefix: "Test test test"
};
```

But what if the backend URL depends on the fact whether the application is run in a development
environment (or locally) or in a production environment.

## Solution
Create a file called `config.in`:

``` javascript
module.exports = {
    BackendURL: "{{{backend_url}}}",
    MaxRetries: 12,
    ErrorPrefix: "Test test test"
};
```

The templates are based on Mustache.

Now you can create a configuration file `in.json` in your project's root:

``` json
{
    "backend_url": {
       "dev": "local-address",
       "prod": "production-address"
    }
}
```
