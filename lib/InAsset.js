const proc = require("process");
const { Asset } = require("parcel-bundler");
const Mustache = require("mustache");

class InAsset extends Asset {
    async parse(code) {
        const config = await this.getConfig(["in.json"]);

        for (let key of Object.keys(config)) {
            const match = proc.env.NODE_ENV === "development" ? (
                config[key]["dev"]
            ) : (
                config[key]["prod"]
            );

            // Replace the template
            code = Mustache.render(code, { [key]: match });
        }

        this.code = code;
        super.parse(this.code);
    }
    
    async generate() {
        return [{
            type: "js",
            value: this.code,
        }]
    }
};

module.exports = InAsset;
